# GoogleDriveUploader

Simple console app for uploading once file to google drive.

All setups by arguments:

"-key-file" = path to file with private services account key;

"-target-file" = path to file whict will upload;

"-dir-id" = google drive target folder id;

"-content-type" = MIME format of content type;


To add as submodule
```
git submodule add https://gitlab.com/wainworkengine/googledriveuploader.git GoogleDriveUploader
```